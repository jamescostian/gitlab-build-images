TOOLS=(DEBIAN RUBY GOLANG GIT LFS CHROME NODE YARN POSTGRESQL GRAPHICSMAGICK PGBOUNCER BAZELISK)

function get_image_name(){
    local IMAGE_NAME
    IMAGE_NAME=""
    for tool in "${TOOLS[@]}"; do
        if [ -n "${!tool}" ]; then
            IMAGE_NAME="${IMAGE_NAME}-${tool,,}-${!tool}"
        fi
    done
    echo "${IMAGE_NAME:1}"
}
